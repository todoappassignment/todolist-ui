import {shallowMount} from "@vue/test-utils";
import TodoItem from "../../src/components/TodoItem"
import mockAxios from "axios"

describe('TodoItem.vue', () => {
    it('should renders todo', function () {
        let todo = {
            'id': 'testTodoId',
            'title': 'Test Todo',
            'completed': false,
        }
        let wrapper = shallowMount(TodoItem, {
            propsData: {todo}
        })
        expect(wrapper.find('.todo-item-label').text()).toEqual('Test Todo')
    });

    it('should renders todo-item-label', function () {
        let todo = {
            'id': 'testTodoId',
            'title': 'Test Todo',
            'completed': false,
        }
        let wrapper = shallowMount(TodoItem, {
            propsData: {todo}
        })

        expect(wrapper.find('.todo-item-label').isVisible())
    });

    it('should renders remove-todo-item element', function () {
        let todo = {
            'id': 'testTodoId',
            'title': 'Test Todo',
            'completed': false,
        }
        let wrapper = shallowMount(TodoItem, {
            propsData: {todo}
        })

        expect(wrapper.find('.remove-todo-item').isVisible())
    });

    it('should renders todo item as completed', function () {
        let todo = {
            'id': 'testTodoId',
            'title': 'Test Todo',
            'completed': true
        }
        let wrapper = shallowMount(TodoItem, {
            propsData: {todo}
        })

        let classes = wrapper.find('.todo-item-label').classes();
        console.log(classes)
        expect(wrapper.find('.todo-item-label').classes().filter(className => className == 'completed').length).toEqual(1)
    });

    it('should complete todo when it has been clicked', async function () {
        mockAxios.put.mockImplementationOnce(() => Promise.resolve({
            data: {
                'id': 'testTodoId',
                'title': 'Test Todo',
                'completed': true
            }
        }))
        let todo = {
            'id': 'testTodoId',
            'title': 'Test Todo',
            'completed': false
        }
        let wrapper = shallowMount(TodoItem, {
            propsData: {todo}
        })

        let todoItem = wrapper.find('.todo-item-label')
        todoItem.trigger('click')
        await wrapper.vm.$nextTick()
        expect(todoItem.classes().filter(className => className == 'completed').length).toEqual(1)
        expect(wrapper.vm.completed).toEqual(true)
        expect(mockAxios.put).toHaveBeenCalledTimes(1)
        expect(mockAxios.put).toBeCalledWith(
            process.env.VUE_APP_API_BASE_URL  + "testTodoId",
            {
                'title': 'Test Todo',
                'completed': true
            }
        )
    });

    it('should in complete todo when it has been clicked on completed state', async function () {
        mockAxios.put.mockImplementationOnce(() => Promise.resolve({
            data: {
                'id': 'testTodoId',
                'title': 'Test Todo',
                'completed': false
            }
        }))
        let todo = {
            'id': 'testTodoId',
            'title': 'Test Todo',
            'completed': true
        }
        let wrapper = shallowMount(TodoItem, {
            propsData: {todo}
        })
        let todoItem = wrapper.find('.todo-item-label')
        expect(todoItem.classes().filter(item => item == 'completed').length).toEqual(1) //completed before action
        todoItem.trigger('click')
        await wrapper.vm.$nextTick()
        expect(todoItem.classes().filter(className => className == 'completed').length).toEqual(0)
        expect(wrapper.vm.completed).toEqual(false)
        expect(mockAxios.put).toBeCalledWith(
            process.env.VUE_APP_API_BASE_URL + "testTodoId",
            {
                'title': 'Test Todo',
                'completed': false
            }
        )
    });

    it('should emit removedTodo event when .remove-todo-item has been clicked', async function () {
        //given
        mockAxios.delete.mockImplementationOnce(() => Promise.resolve())
        let todo = {
            'id': 'testTodoId'
        }
        let wrapper = shallowMount(TodoItem, {
            propsData: {todo}
        });

        //when
        let removeTodoBtn = wrapper.find('.remove-todo-item');
        removeTodoBtn.trigger('click')
        await wrapper.vm.$nextTick()

        //then
        expect(wrapper.emitted().removedTodo).toBeTruthy()
        expect(wrapper.emitted().removedTodo.length).toBe(1)
    });

    it('should delete todo event when .remove-todo-item has been clicked', async function () {
        //given
        mockAxios.delete.mockImplementationOnce(() => Promise.resolve())
        let todo = {
            'id': 'testTodoId'
        }
        let wrapper = shallowMount(TodoItem, {
            propsData: {todo}
        });

        //when
        let removeTodoBtn = wrapper.find('.remove-todo-item');
        removeTodoBtn.trigger('click')
        await wrapper.vm.$nextTick()

        //then
        expect(mockAxios.delete).toHaveBeenCalledWith(process.env.VUE_APP_API_BASE_URL + 'testTodoId')
    });

})