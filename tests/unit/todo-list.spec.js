import {mount} from "@vue/test-utils";
import {shallowMount} from "@vue/test-utils";
import TodoList from "../../src/components/TodoList";
import mockAxios from "axios"

describe('TodoList.vue', () => {
    it('should renders input text with new-todo-input id', function () {
        let wrapper = shallowMount(TodoList, {})
        expect(wrapper.find('#new-todo-input').isVisible())
    })

    it('should renders button with "Add" text and "add-todo-btn" id',  function () {
        let wrapper = shallowMount(TodoList)
        expect(wrapper.find('#add-todo-btn').text()).toEqual('Add')
    });

    it('should renders todo-list', () => {
        let wrapper = shallowMount(TodoList)
        expect(wrapper.find('#todo-items').isVisible())
    });

    it('should not add empty todo input into todo list when #add-todo-btn has been clicked', async function () {
        let wrapper = shallowMount(TodoList, {
            data: function () {
                return {
                    newTodo: ''
                }
            }
        })
        expect(wrapper.vm.todoList.length).toEqual(0)
        let addTodoButton = wrapper.find('#add-todo-btn')
        addTodoButton.trigger('click')
        await wrapper.vm.$nextTick()
        expect(wrapper.vm.todoList.length).toEqual(0)
        expect(mockAxios.post).toHaveBeenCalledTimes(0)
    });

    it('should add typed todo into todo list when #add-todo-btn has been clicked', async function () {
        mockAxios.post.mockImplementationOnce(() => Promise.resolve({
            data: {
                'id': '12345',
                'title': 'Test Todo',
                'completed': false
            }
        }));
        let wrapper = mount(TodoList, {
            data: function () {
                return {
                    newTodo: 'Test Todo'
                }
            }
        });

        expect(wrapper.vm.todoList.length).toEqual(0)
        let addTodoButton = wrapper.find('#add-todo-btn')
        addTodoButton.trigger('click')
        await wrapper.vm.$nextTick()
        expect(wrapper.vm.todoList.length).toEqual(1)
        expect(wrapper.vm.todoList[0].title).toEqual('Test Todo')

        expect(mockAxios.post).toHaveBeenCalledTimes(1)
        expect(mockAxios.post).toHaveBeenCalledWith(
            process.env.VUE_APP_API_BASE_URL,
            {'title': 'Test Todo'}
        )
    });

    it('should delete todo when removedTodo event has been emits',  async function () {
        //given
        mockAxios.delete.mockImplementationOnce(() => Promise.resolve())
        let todoList = [
            {
                'id': 'testTodoId'
            }
        ]
        let wrapper = mount(TodoList, {
            data: function () {
                return {
                    todoList: todoList
                }
            }
        })

        //when
        wrapper.vm.$emit('removedTodo', todoList[0])
        await wrapper.vm.$nextTick()

        //then
        expect(wrapper.vm.todoList.length).toEqual(0)
    });
})
