FROM node:13-alpine
WORKDIR /todolist-ui
COPY . .
RUN npm install
CMD npm run serve