# TodoList UI
TodoList Frontend Project.

| Project link:| [http://35.228.252.73](http://35.228.252.73) |
| -----------  | ----------- |

## Requirements
- [npm](https://www.npmjs.com/)

## Features
* Add new todo
* Delete todo from the list
* Complete/Incomplete todo

## Tech Stack
* Vue: 2.6.11
* Axios: 0.19.2
* Vue Test Utils: 1.0.0-beta.31
* Vue Unit Test Jest Plugin: 4.3.0

## Project Structure

```
.
├── public                   
│   ├── index.html           
├── src           
|   ├── mocks          # test mocks (e.g. mock axios)
│   ├── components     # UI components
|   ├── service        # Api calls
│   ├── App.vue               
│   ├── main.js
├── tests              
│   ├── unit           # Unit tests
├── .gitlab-ci.yml     # Pipeline file
├── Dockerfile
├── k8s                # Kubernetes config files
├── README.md
├── package.json
```
## Build and Tests
### Building the project

Project setup
```
$ npm install
```

Compiles and hot-reloads for development
```
$ npm run serve
```

Compiles and minifies for production
```
$ npm run build
```

Lints and fixes files
```
$ npm run lint
```

### Running tests 

Run your unit tests
```
$ npm run test:unit
```

## Deploy Project 
[Gitlab CI/CD pipeline](.gitlab-ci.yml) is defined with 4 stage. (build, test, dockerize, deploy) And triggers acceptance tests after deploy stage